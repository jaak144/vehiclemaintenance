package ee.valiit.VM.Controller;

import ee.valiit.VM.Model.WorkInput;
import ee.valiit.VM.repository.ProjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/")
@CrossOrigin("*")
public class ProjektController {

    @Autowired
    private ProjektRepository projektRepository;

    @GetMapping("/sisendinfo")
    private List<WorkInput> getWorkLines() {
        return projektRepository.fetchWorkLines();
    }

    @GetMapping("/projekt")
    public WorkInput getWorkLine(@RequestParam("id") int id) {
        return projektRepository.fetchWorkLine(id);
    }

    @DeleteMapping("/sisendinfo")
    public void deleteWorkLine(@RequestParam("id") int id) {
        projektRepository.deleteWorkLine(id);
    }

    @PostMapping("/sisendinfo")
    public void addWorkLine(@RequestBody WorkInput x) {
        projektRepository.addWorkLine(x);
    }

    @PutMapping("/sisendinfo")
    public void updateWorkLine(@RequestBody WorkInput y) {
        projektRepository.updateWorkLine(y);
    }
}
