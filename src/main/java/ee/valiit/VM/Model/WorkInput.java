package ee.valiit.VM.Model;

public class WorkInput {
    private int ID;
    private int year;
    private int km;
    private String work;
    private String producer;
    private int price;
    private String workType;
    private String notes;

    public WorkInput() {
    }

    public WorkInput(int ID, int year, int km, String work, String producer, int price, String workType, String notes) {
        this.ID = ID;
        this.year = year;
        this.km = km;
        this.work = work;
        this.producer = producer;
        this.price = price;
        this.workType = workType;
        this.notes = notes;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
