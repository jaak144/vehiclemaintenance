package ee.valiit.VM.repository;

import ee.valiit.VM.Model.WorkInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProjektRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public WorkInput fetchWorkLine(int id) {
        List<WorkInput> workLines = jdbcTemplate.query(
                "SELECT * FROM sisendinfo WHERE id = ?",
                new Object[]{id},
                (row, rowNum) -> {
                    return new WorkInput(
                            row.getInt("id"),
                            row.getInt("year"),
                            row.getInt("km"),
                            row.getString("work"),
                            row.getString("producer"),
                            row.getInt("price"),
                            row.getString("workType"),
                            row.getString("notes")
                    );
                }
        ); return workLines.size() > 0 ? workLines.get(0) : null;
   }

    public List<WorkInput> fetchWorkLines() {
        List<WorkInput> workLines = jdbcTemplate.query(
                "SELECT * FROM sisendinfo",
                (row, rowNum) -> {
                    return new WorkInput(
                            row.getInt("id"),
                            row.getInt("year"),
                            row.getInt("km"),
                            row.getString("work"),
                            row.getString("producer"),
                            row.getInt("price"),
                            row.getString("workType"),
                            row.getString("notes")
                    );
                }
        );
        return workLines;
    }
    public void deleteWorkLine(int id){
        jdbcTemplate.update("DELETE FROM sisendinfo WHERE id = ?", id);
    }

    public void addWorkLine(WorkInput workInput) {
        int year = workInput.getYear();
        int km = workInput.getKm();
        String work = workInput.getWork();
        String producer = workInput.getProducer();
        int price = workInput.getPrice();
        String workType = workInput.getWorkType();
        String notes = workInput.getNotes();


        jdbcTemplate.update("INSERT INTO sisendinfo (year, km, work, producer, price, workType, notes) VALUES (?, ?, ?, ?, ?, ?, ?)", year, km, work, producer, price, workType, notes);
    }

    public void updateWorkLine(WorkInput workInput) {
        jdbcTemplate.update("UPDATE sisendinfo SET year = ?, km = ?, work = ?, producer = ?, price = ?, workType = ?, notes = ? WHERE id = ?",
                workInput.getYear(), workInput.getKm(),workInput.getWork(), workInput.getProducer(), workInput.getPrice(), workInput.getWorkType(), workInput.getNotes(), workInput.getID());
    }


}
