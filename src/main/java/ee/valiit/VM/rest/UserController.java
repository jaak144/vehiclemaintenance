package ee.valiit.VM.rest;

import ee.valiit.VM.dto.GenericResponseDto;
import ee.valiit.VM.dto.JwtRequestDto;
import ee.valiit.VM.dto.JwtResponseDto;
import ee.valiit.VM.dto.UserRegistrationDto;
import ee.valiit.VM.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}
