package ee.valiit.VM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class VmApplication {

	public static void main(String[] args) {
		SpringApplication.run(VmApplication.class, args);
	}

	@Bean // Peale security sõltuvuste allatõmbamist lisada see bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	// Seejärel lood 'user' kataloogi SQL baasi kopeerides järgmise sisu Querysse:

	//	CREATE TABLE `user` (
	//			`id` INT NOT NULL AUTO_INCREMENT,
	//    `username` VARCHAR(190) NOT NULL,
	//    `password` VARCHAR(190) NOT NULL,
	//	PRIMARY KEY (id),
	//	UNIQUE(username)
	//);
	//
	//	INSERT INTO `user` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');sisendinfosisendinfosisendinfo

	// F5 toob loodud tabeli databaasis esile.
}
